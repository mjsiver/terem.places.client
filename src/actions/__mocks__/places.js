
import axios from 'axios';

jest.mock('axios');

const mockPlaces = [
  {
      "title": "I am the greatest",
      "img": "https://i.pinimg.com/originals/ee/f2/2f/eef22f4add36ba425285c824188abf3e.jpg",
      "location": "My Attitude"
  },
  {
      "title": "I am the best",
      "img": "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR0sIkYGitWUCdnoQuwN8Icj15tTy98kqV3aw&usqp=CAU",
      "location": "My Skill"
  }
];

export function getPlaces() {
  return new Promise ((resolve) => {
    
    // act
    axios.get.mockImplementationOnce(() => 
      Promise.resolve({data: Object.assign([], mockPlaces)})
    );

    resolve(
      axios.get()
      .then(places => {
        return places;
      }));
  });
}
