/* component to display the each place as a tile */

import React from 'react';
import './styles/tile.css';

const Tile = (props) => {

  return (
    <div className={`tile ${(props.hide ? 'hide': '')}`} >
      <img src={props.img} alt={props.title} loading='lazy' width={props.width} height={props.height} />
      <div className='tile-caption'>
        <h5>{props.title}</h5>
        <span>{props.location}</span>
      </div>
    </div>
  )
}

export default Tile;